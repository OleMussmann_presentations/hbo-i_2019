# HBO-I 2019

Presentation for the **HBO-I event** in Utrecht.

Files and media are licensed under [CC0](https://creativecommons.org/share-your-work/public-domain/cc0/), except where mentioned in the [NOTICE](NOTICE) file.

## Setup

Create the folder structure shown below. Clone the [reveal.js](https://github.com/hakimel/reveal.js) repo. Make a symlink to the root folder of this project. This keeps your own code separate from the reveal.js (and your own css) versions.

### Reveal.js folder structure

```
[reveal.js_path]
├── 3.8.0              <- create this folder, check version number
|   └──reveal.js       <- clone the reveal.js repo from https://github.com/hakimel/reveal.js
└── custom_css         <- create this folder
    └──cbs_3.8.0.css   <- this is the place for your own css tweaks, include version number
```

### Project folder structure

```
this project
├── files
├── index.html
├── LICENSE
├── NOTICE
├── README.md
└── reveal.js -> [reveal.js_path]/  <- create a symlink "reveal.js" to the reveal.js root folder, see above
```

## Develop

In the root folder of this project, start a web server. This can be as simple as:
```
python3 -m http.server
```
Open the presentation in your browser. Edit the index.html, refresh browser. Repeat.
